package com.xadmin.SpringBootCrud.bean;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.Table;

@Entity
@Table(name="Subject")
public class Subject {
	
	public Subject() {
		super();
	}
	public Subject(String name, String id, String dept, String clas, String year, String address, String phoneno,
			String emergency_contact, String dob, String bgroup, String regdate) {
		super();
		this.name = name;
		this.id = id;
		this.dept = dept;
		this.clas = clas;
		this.year = year;
		this.address = address;
		this.phoneno = phoneno;
		this.emergency_contact = emergency_contact;
		this.dob = dob;
		this.bgroup = bgroup;
		this.regdate = regdate;
	}
	private String name;
	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "my_entity_seq")
	private String id;
	private String dept;
	private String clas;
	private String year;
	private String address;
	private String phoneno;
	private String emergency_contact;
	private String dob;
	private String bgroup;
	private String regdate;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	public String getDept() {
		return dept;
	}
	public void setDept(String dept) {
		this.dept = dept;
	}
	public String getClas() {
		return clas;
	}
	public void setClas(String clas) {
		this.clas = clas;
	}
	public String getYear() {
		return year;
	}
	public void setYear(String year) {
		this.year = year;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getPhoneno() {
		return phoneno;
	}
	public void setPhoneno(String phoneno) {
		this.phoneno = phoneno;
	}
	public String getEmergency_contact() {
		return emergency_contact;
	}
	public void setEmergency_contact(String emergency_contact) {
		this.emergency_contact = emergency_contact;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public String getBgroup() {
		return bgroup;
	}
	public void setBgroup(String bgroup) {
		this.bgroup = bgroup;
	}
	public String getRegdate() {
		return regdate;
	}
	public void setRegdate(String regdate) {
		this.regdate = regdate;
	}
	

}
